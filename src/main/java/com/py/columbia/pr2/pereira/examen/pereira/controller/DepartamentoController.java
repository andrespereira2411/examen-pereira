
package com.py.columbia.pr2.pereira.examen.pereira.controller;

import com.py.columbia.pr2.pereira.examen.pereira.repository.DepartamentoRepository;
import com.py.columbia.pr2.pereira.examen.pereira.repository.EmpleadoRepository;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartamentoController {
    
    private DepartamentoRepository departamentoRepository;
    private EmpleadoRepository empleadoRepository;
    
    public DepartamentoController(DepartamentoRepository departamentoRepository, EmpleadoRepository empleadoRepository){ 
        this.departamentoRepository = departamentoRepository;
        this.empleadoRepository = empleadoRepository;
  
   }
}

