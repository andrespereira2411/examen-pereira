
package com.py.columbia.pr2.pereira.examen.pereira.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
//import javax.persistence.*;


@Entity
public class Empleado {
    @Id
    @GeneratedValue (generator = "generator_empleado")
    @SequenceGenerator (
            name = "generator_empleado",
            sequenceName = "empleado_sequence_id",
            initialValue = 100)

  //)
   private Long id;

  @NotBlank
   @Size(min = 3, max = 100)
    private String titulo;

    @Column (columnDefinition = "text")
    private String descripcion; 
}