
package com.py.columbia.pr2.pereira.examen.pereira.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.SequenceGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


public class Departamento  {
    @Id
    @GeneratedValue (generator = "generator_departamento")
    @SequenceGenerator(
            name =  "generator_departamento",
            sequenceName = "departamento_sequence_id",
            initialValue = 100
    )
    private Long id;
    
    @Column (columnDefinition = "text")
    private String texto;
    
 //   @ManyToOne (fetch = FechType.LAZY, optional = false)
    @JoinColumn(name = "empleado_id", nullable = false)
    @OnDelete (action = OnDeleteAction.CASCADE)
    @JsonIgnore 
    private Departamento departamento;
   // private Object FechType;
}
