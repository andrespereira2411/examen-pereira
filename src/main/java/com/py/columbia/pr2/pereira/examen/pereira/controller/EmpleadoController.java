
package com.py.columbia.pr2.pereira.examen.pereira.controller;


import com.py.columbia.pr2.pereira.examen.pereira.model.Empleado;
import com.py.columbia.pr2.pereira.examen.pereira.repository.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;



 
@RestController
@RequestMapping({"/empleados"})
public class EmpleadoController {
    
    private EmpleadoRepository empleadoRepository;
    
    public EmpleadoController (EmpleadoRepository empleadoRepository){
        this.empleadoRepository = empleadoRepository;
    }
    
    @GetMapping 
    public Page<Empleado>getEmpleados (Pageable pageable) {
        return empleadoRepository.findAll(pageable);
    }
    
    @PostMapping
    public Empleado crearEmpleado (@Valid @RequestBody Empleado empleado){
        return empleadoRepository.save(empleado);}
    }
    //@PutMapping ("/{EmpleadoId}")
    //public Empleado actualizarEmpleado( @PathVariable Long empleadoId,@Valid @RequestBody Empleado empleadoRequest) {
      //  return empleadoRepository.findById(empleadoId)
        //        .map(empleadoBD -> {
          //          empleadoBD.setTitulo (empleadoRequest.getTitulo());
            //        empleadoBD.setDescripcion(empleadoRequest.getDescripcion());
              //      return empleadoRepository.save(empleadoBD);
      // }).orElseThrow(()-> new RecursoNoEncontradoException("No existe la pregunta con id " + empleadoId));
        
    //}
      // @DeleteMapping ("/{preguntaId}")
     //  public ResponseEntity <?> eliminarEmpleado (@PathVariable Long preguntaId){
       // return empleadoRepository.finById(empleadoId)
               // .map(empleadoBD -> {
                  //  empleadoRepository.delete(empleadoBD);
                  //  return ResponseEntity.ok().build();
                 //  }).orElseThrow(() -> new RecursoNoEncontradoException ("No existe la pregunta con id " + empleadoId)
  //    }
   // }
// }
