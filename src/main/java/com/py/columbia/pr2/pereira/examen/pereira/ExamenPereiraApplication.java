package com.py.columbia.pr2.pereira.examen.pereira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ExamenPereiraApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenPereiraApplication.class, args);
	}

}
