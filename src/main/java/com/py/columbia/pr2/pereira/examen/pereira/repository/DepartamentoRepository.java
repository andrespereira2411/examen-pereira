
package com.py.columbia.pr2.pereira.examen.pereira.repository;

import com.py.columbia.pr2.pereira.examen.pereira.model.Departamento;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DepartamentoRepository extends JpaRepository <Departamento,Long>{
    List<Departamento> findByPreguntaId (Long id);
    // aca tendria que ser Page en vez de List//
   
}
