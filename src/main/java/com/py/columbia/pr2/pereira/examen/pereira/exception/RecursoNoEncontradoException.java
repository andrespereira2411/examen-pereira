
package com.py.columbia.pr2.pereira.examen.pereira.exception;

import sun.security.provider.certpath.OCSPResponse.ResponseStatus;

//@ResponseStatus (HttpStatus.NOT_FOUND)
public class RecursoNoEncontradoException extends RuntimeException {
   //rivate Object HttpStatus;
    
    public RecursoNoEncontradoException (String message){
    super (message);
}

public RecursoNoEncontradoException (String message, Throwable cause){
    super (message,cause);    
}
}